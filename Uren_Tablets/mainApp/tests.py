# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

# Create your tests here.


"SELECT KIT.RegistratieTabel.RegistratieID, KIT_beheer.werknemers.Voornaam, KIT_beheer.opdrachten.ordernummer,"
"KIT.RegistratieTabel.Starttijd, KIT_beheer.debiteuren.bedrijf"
"FROM (KIT_beheer.werknemers INNER JOIN KIT.RegistratieTabel"
"ON KIT_beheer.werknemers.Werknemernummer = KIT.RegistratieTabel.Werknemer)"
"LEFT JOIN KIT_beheer.opdrachten"
"ON KIT.RegistratieTabel.Opdracht = KIT_beheer.opdrachten.opdrachtid"
"LEFT JOIN KIT_beheer.debiteuren"
"ON KIT_beheer.opdrachten.klantnr = KIT_beheer.debiteuren.klantnr"
"WHERE (((KIT.RegistratieTabel.Actief)=-1))")