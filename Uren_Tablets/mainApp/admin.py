# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Register, Employees, Debiteuren1

admin.site.register(Register)
admin.site.register(Employees)
# admin.site.register(Opdrachten1)
admin.site.register(Debiteuren1)


# Register your models here.
