# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from forms import InputForm
from . import forms
from models import Employees
from mysql.connector import MySQLConnection, Error
from python_mysql_dbconfig import read_db_config
from datetime import datetime
from django.forms import ValidationError
from django.contrib import messages 
from django.http import JsonResponse


# def index(request):
#     my_dict = {'insert_content': "Hello im from firstapp (views)",
#     'insert_content2': "Hello im seccond line",
    
#     }
#     return render (request, 'mainApp/index.html', context=my_dict)

def InputFormView(request):
    def connect():
        """ Connect to MySQL database """
    
        db_config = read_db_config()
    
        try:
            print('Connecting to MySQL database...')
            conn = MySQLConnection(**db_config)
    
            if conn.is_connected():
                print('connection established.')
            else:
                print('connection failed.')
    
        except Error as error:
            print(error)
    
        finally:
            conn.close()
            print('Connection closed.')

    def query_get_names():
        results = []
        try:
            dbconfig = read_db_config()
            conn = MySQLConnection(**dbconfig)
            cursor = conn.cursor(dictionary=True)
            cursor.execute("SELECT Werknemernummer, voornaam FROM werknemers WHERE werkvloer = '-1'")
    
            results =  cursor.fetchall()

            # row = cursor.fetchone()
    
            # while row is not None:
            #     print(row)
            #     results.append(row)
            #     row = cursor.fetchone()
                
    
        except Error as e:
            print(e)
    
        finally:
            cursor.close()
            conn.close()
            return results

    def query_get_projects():
        results = []
        try:
            dbconfig = read_db_config()
            conn = MySQLConnection(**dbconfig)
            cursor = conn.cursor(dictionary=True)
            # cursor.execute("SELECT opdrachtid, ordernummer FROM opdrachten WHERE gereed='0'")
            cursor.execute("SELECT ordernummer, opdrachtid FROM opdrachten WHERE gereed='0'")
    
            results =  cursor.fetchall()
            # row = cursor.fetchone()
    
            # while row is not None:
            #     results.append(str(row[0]))
            #     row = cursor.fetchone()
                
    
        except Error as e:
            print(e)
    
        finally:
            cursor.close()
            conn.close()
            return results

    def query_get_register():
        results = []
        try:
            dbconfig = read_db_config()
            conn = MySQLConnection(**dbconfig)
            cursor = conn.cursor(dictionary=True)
            # cursor.execute("SELECT * FROM RegistratieTabel WHERE Actief='-1'")
    
            cursor.execute("SELECT KIT.RegistratieTabel.RegistratieID, KIT_beheer.werknemers.Voornaam, KIT_beheer.opdrachten.ordernummer, "
                          "KIT.RegistratieTabel.Starttijd, KIT_beheer.debiteuren.bedrijf "
                          "FROM KIT.RegistratieTabel "
                          "LEFT JOIN KIT_beheer.werknemers "
                          "ON KIT_beheer.werknemers.Werknemernummer = KIT.RegistratieTabel.Werknemer "
                          "LEFT JOIN KIT_beheer.opdrachten "
                          "ON KIT.RegistratieTabel.Opdracht = KIT_beheer.opdrachten.opdrachtid "
                          "LEFT JOIN KIT_beheer.debiteuren "
                          "ON KIT_beheer.opdrachten.klantnr = KIT_beheer.debiteuren.klantnr "
                          "WHERE (((KIT.RegistratieTabel.Actief)=-1))")

            row = cursor.fetchone()
    
            while row is not None:
                print(row)
                results.append(row)
                row = cursor.fetchone()
                
    
        except Error as e:
            print(e)
    
        finally:
            cursor.close()
            conn.close()
            return results

    def query_get_weekno():
        results = []
        try:
            dbconfig = read_db_config()
            conn = MySQLConnection(**dbconfig)
            cursor = conn.cursor()
            # cursor.execute("SELECT * FROM RegistratieTabel WHERE Actief='-1'")
    
            cursor.execute("Select KIT_beheer.werkbare_dagen.produktieweek "
                          "From KIT_beheer.werkbare_dagen "
                          "Where datum = CURDATE()")

            row = cursor.fetchone()
    
            while row is not None:
                print(row)
                results.append(row)
                row = cursor.fetchone()
                
    
        except Error as e:
            print(e)
    
        finally:
            cursor.close()
            conn.close()
            value, = results[0]
            return value

    def query_set_record(userno, task, projectno):
        print("STARTING RECORD: " + str(userno) + " , " + task + " , " + project)

        query = "INSERT INTO KIT.RegistratieTabel (Werknemer, Opdracht, Productieweek, Datum, Starttijd, Omschrijving, Actief) "\
                "VALUES (%s, %s, %s, CURDATE(), NOW(), %s, '-1')" 
                
        args = (userno, projectno, query_get_weekno(), task)
    
        try:
            db_config = read_db_config()
            conn = MySQLConnection(**db_config)
    
            cursor = conn.cursor()
            cursor.execute(query, args)
    
            if cursor.lastrowid:
                print('last insert id', cursor.lastrowid)
            else:
                print('last insert id not found')
    
            conn.commit()
        except Error as error:
            print(error)
    
        finally:
            cursor.close()
            conn.close()

    def query_set_end_record(record):
        print("ENDING RECORD: " + record )
        #   query.prepare(
        # query.bindValue(":I", task)
        

        # read database configuration
        db_config = read_db_config()
    
        # prepare query and data
        query = "UPDATE KIT.RegistratieTabel "\
                "SET Duur = (TIMESTAMPDIFF(second, Starttijd, NOW())/3600), Actief='0' "\
                "WHERE RegistratieID = %s "\
                
    
        data = (record,)
        # print(data)

        print(query, data)

        try:
            conn = MySQLConnection(**db_config)
    
            # update book title
            cursor = conn.cursor()
            cursor.execute(query, data)
    
            # accept the changes
            conn.commit()
    
        except Error as error:
            print(error)
    
        finally:
            cursor.close()
            conn.close()

    projects_list = query_get_projects()
    names_list = query_get_names()

    # data conditioning. remove 'werknemernummer' and remove unicode. 
    projects = [str(item['ordernummer']) for item in projects_list]
    names = [item['voornaam'].encode("utf-8") for item in names_list]
    print(projects)
    print(names)
    register = query_get_register()
    # print(register)
    # week = query_get_weekno()

    #return JsonResponse({'message':'OK'},status=400)

    if request.method == 'POST':
        print("form post")
        # print(request.POST.get['inp_user', '0'])

        form1 = InputForm(request.POST)
        if '#mainForm' in request.POST:
            print ("main form")
        
        if '\u201dmy_id\u201d' in request.POST:

            # print("ASDKSADFKASDFKASDFKASDKFAKSDFKAASDKF")
            alldata = request.POST
            # print('alldata' + str(alldata))
            data = alldata.get("inp_task", "0")
            task =  alldata.get("inp_task", "0")
            user =  alldata.get("inp_user", "0")
            project = alldata.get("inp_project", "0")

            if project == "":
                return JsonResponse({'message':'Project-field empty'},status=404)
            elif user == "":
                return JsonResponse({'message':'User-field empty'},status=404)
            elif task == "":
                return JsonResponse({'message':'Task-field empty'},status=404)

            if user not in names:
                return JsonResponse({'message':'User unknown'},status=404)

            if project not in projects:
                return JsonResponse({'message':'project unknown'},status=404)       
            
            # Check of user al actief is:
            for i in register:
                # print(i)
                if i.get('Voornaam') == user:
                    return JsonResponse({'message':'User al geregistreerd'},status=404)

            # if user in register.Voornaam:
            #     return JsonResponse({'message':'User al geregistreerd'},status=404)
            # print(projects_list)
            
            for i in projects_list:
                print(i.get('ordernummer'))
                if str(i.get('ordernummer')) == project:
                    
                    projectno = str(i.get('opdrachtid'))

            for i in names_list:
                print(i.get('voornaam'))
                if i.get('voornaam') == user:
                    
                    userno = i.get('Werknemernummer')
            
                

            query_set_record(userno, task, projectno)


            # else:

            #     return JsonResponse({'message':'wel gestuurd maar niet correct'},status=404)

            # print("wanna start???")
            # messages.error(request,'username or password not correct')
            
            # messages.error(request, "Error")
        
            # print(query_get_weekno())
            return JsonResponse({'message':'Dit lijkt goed te komen'},status=200)

        elif 'tegel' in request.POST:
            #labelid = request.GET.get("id", False)
            # print(request) 
            # print("je hebt op een tegel gedrukt")
            id = request.POST['tegel']
            
            # print(week)
            for i in register:
                # print("REGID = " + str(i.get('RegistratieID')) )
                # temp = i.get('RegistratieID')
                if id == str(i.get('RegistratieID')):
                    # print("ID =" + str(id))
                    # print("ALL = " + str(no))
                    query_set_end_record(id)

                    register = query_get_register()

                # if no == id:
                #     name = register[no]
                #     print name

                # if key in request.POST:
                #     print(key)

        else:

            print("form post")
            form1 = InputForm(request.POST)
            
            
            if form1.is_valid():
                name = form1.cleaned_data.get('name')
                task = form1.cleaned_data.get('task')
                project = form1.cleaned_data.get('project')

                # Entry = Employees(name=name, id_mysql="199")
                # Entry.save()
                # print(name)
                
                # return HttpResponseRedirect('')
                #return JsonResponse({'message':'Item saved'},status=201)
                #return HttpResponseRedirect('/thanks/')

            else:
                 
                # print("no valid??")
                return JsonResponse({'message':'kan geen recoord aanmaken'},status=404)

    else:
        # print("not post")
        
        #return JsonResponse({'message':'kan geen record aanmaken'})

        form1 = InputForm()
    # toppings = [ 'cheese','tomatos','pineapple' ]
    # countries = ["Afghanistan", "Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

    #return JsonResponse({'message':'kan geen record aanmaken'})

    return render(request, 'mainApp/index.html', {'form1': form1, 'names': names, 'projects': projects, 'register': register,  })

    
 

# def addgame(request):
#     if request.method == "POST":
#         print("post")
#         form = InputForm()
#         if form.is_valid():
#             print("valid")
#             infolist = form.save(commit=False)
#             infolist.created_date = timezone.now()
#             infolist.save()
#             return redirect('index')
#     else:
#         pass
#         form = InputForm()
#         print("not post")
#     return render(request, 'mainApp/form_page.html', {'form': form})