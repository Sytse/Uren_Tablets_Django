from django.conf.urls import url
from django.contrib import admin
from mainApp import views


urlpatterns = [
    # url(r'^$', views.index, name='index'),
    url(r'^$', views.InputFormView, name='form_name'),
    url(r'formpage/', views.InputFormView, name='form_name'),
    url(r'^admin/', admin.site.urls),
]
 