function autocomplete(inp, arr, option) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/

  // var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      console.log("eventlistner input");
      console.log("inp="  + inp.id);
      console.log("option=" + option);
   // var textBox = document.getElementById("input_project");
            showAutocomplete(inp,e, option);

  });

  inp.addEventListener("focus", function(e) {
      console.log("eventlistner focus");
       console.log("inp="  + inp.id);
        console.log("optiom=" + option);
   // var textBox = document.getElementById("input_project");
       showAutocomplete(inp,e, option);
  });

  /*execute a function presses a key on the keyboard:*/
  // inp.addEventListener("keydown", function(e) {
  inp.addEventListener("keydown", function(e) {
      handleAutocomplete(this,e);
  });

  function showAutocomplete(inputElement, e, option) {
      console.log("option 1 executed");
      var a, b, i, val = inputElement.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", inputElement.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      inputElement.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/

        if (option == 1){
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/


          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/


          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
    }else{

        for (i = 0; i < arr.length; i++) {
          /*check if the item starts with the same letters as the text field value:*/
          var str = arr[i].toUpperCase();
          val = val.toUpperCase();
          // console.log("str=" + str);
          // console.log("val=" + val);
          var n = str.indexOf(val);
          // console.log("n=" + n);
          if (n != -1) {
              // console.log("String =" + str);
            /*create a DIV element for each matching element:*/
            b = document.createElement("DIV");
            /*make the matching letters bold:*/
            b.innerHTML = arr[i].substr(0, n);
            b.innerHTML += "<strong>" + arr[i].substr(n, val.length) + "</strong>";
            b.innerHTML += arr[i].substr(n + val.length);
            /*insert a input field that will hold the current array item's value:*/
            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
            /*execute a function when someone clicks on the item value (DIV element):*/
            b.addEventListener("click", function(e) {
                /*insert the value for the autocomplete text field:*/
                inp.value = this.getElementsByTagName("input")[0].value;
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
            });
            a.appendChild(b);
          }
      }


    }
      }
  }

  function showAutocomplete2(inputElement, e) {

      inputElement.focus();
      console.log("option 2 executed");
      console.log("inputelement= " + inputElement.id);
      var a, b, i, val = inputElement.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", inputElement.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      console.log("a= "+ a.id);
      /*append the DIV element as a child of the autocomplete container:*/
      inputElement.parentNode.appendChild(a);
      /*for each item in the array...*/

  }


  function handleAutocomplete(box,e) {
      console.log("keydown:" + box + "e: " + e);
      var x = document.getElementById(box.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  }


  function addActive(x) {
      console.log("addActive");
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
      });
}
