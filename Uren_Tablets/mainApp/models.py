# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models



class Employees(models.Model):
    name = models.TextField(max_length=50)
    id_mysql = models.IntegerField()


class Register(models.Model):
    author = models.ForeignKey('Employees', on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    stop_time = models.DateTimeField()
    time_dif = models.IntegerField()


    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

class Debiteuren1(models.Model):
    klantnr = models.AutoField(primary_key=True)
    debnummer = models.CharField(unique=True, max_length=50, blank=True, null=True)
    zoekcode = models.CharField(unique=True, max_length=50)
    bedrijf = models.CharField(max_length=50, blank=True, null=True)
    labeltekst = models.CharField(db_column='Labeltekst', max_length=50, blank=True, null=True)  # Field name made lowercase.
    padres = models.CharField(max_length=50, blank=True, null=True)
    ppostwoon = models.CharField(max_length=50, blank=True, null=True)
    badres = models.CharField(max_length=50, blank=True, null=True)
    bpostwoon = models.CharField(max_length=50, blank=True, null=True)
    netnummertel = models.CharField(max_length=15, blank=True, null=True)
    telnummer = models.CharField(max_length=15, blank=True, null=True)
    netnummerfax = models.CharField(max_length=15, blank=True, null=True)
    faxnummer = models.CharField(max_length=15, blank=True, null=True)
    mobiel_nummer = models.CharField(db_column='mobiel nummer', max_length=25, blank=True, null=True)  # Field renamed to remove unsuitable characters.
    email = models.CharField(max_length=50, blank=True, null=True)
    mailing = models.IntegerField(blank=True, null=True)
    aard_bedrijf = models.CharField(db_column='aard bedrijf', max_length=50, blank=True, null=True)  # Field renamed to remove unsuitable characters.
    opmerkingen = models.TextField(blank=True, null=True)
    geen_btw = models.IntegerField(db_column='geen btw', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    btw_nummer = models.CharField(db_column='btw-nummer', max_length=15, blank=True, null=True)  # Field renamed to remove unsuitable characters.
    catalogus = models.IntegerField(blank=True, null=True)
    invoerdatum = models.DateTimeField(blank=True, null=True)
    onbetrouwbaar_field = models.IntegerField(db_column='onbetrouwbaar?', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    laatste_bezoekdatum = models.DateTimeField(db_column='laatste bezoekdatum', blank=True, null=True)  # Field renamed to remove unsuitable characters.
    logo = models.TextField(blank=True, null=True)
    bestelinterval = models.IntegerField(blank=True, null=True)
    afspraken = models.TextField(blank=True, null=True)
    bezoekinterval = models.IntegerField(blank=True, null=True)
    buitenland = models.IntegerField(db_column='Buitenland', blank=True, null=True)  # Field name made lowercase.
    betalingstermijn = models.IntegerField(db_column='Betalingstermijn', blank=True, null=True)  # Field name made lowercase.
    blnafspraak = models.IntegerField(blank=True, null=True)
    davilexid = models.IntegerField(db_column='DavilexId', blank=True, null=True)  # Field name made lowercase.
    digitalefactuur = models.IntegerField(db_column='DigitaleFactuur', blank=True, null=True)  # Field name made lowercase.
    emaildigitalefactuur = models.CharField(db_column='emailDigitaleFactuur', max_length=150, blank=True, null=True)  # Field name made lowercase.
    orderkosten = models.IntegerField(db_column='OrderKosten', blank=True, null=True)  # Field name made lowercase.
    flag = models.IntegerField(db_column='Flag', blank=True, null=True)  # Field name made lowercase.
    timestamp = models.DateTimeField(db_column='Timestamp', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        # managed = False
        db_table = 'debiteuren1'



